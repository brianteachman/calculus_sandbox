import unittest
from math import pi, sin
from Integral import Integral, trapezoidal

# trapezoidal(f, a, x, n)
# Integral(f, a, n=100)

class IntegralTestCase(unittest.TestCase):
    def test_integral_equals_trapezoidal_over_same_range(self):
        y1 = trapezoidal(sin, 0, 2*pi, 200)
        F = Integral(sin, 0, 200)
        y2 = F(2*pi)
        self.assertEqual(y1, y2)

    def test_integral_against_knowns(self):
        F = Integral(sin, 0, 200)

        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
