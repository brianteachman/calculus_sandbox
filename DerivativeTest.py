import unittest
from math import exp, pi, sin, cos, tan
from Derivative import Derivative, SymbolicDerivative


class DerivativeTestCase(unittest.TestCase):

    def test_derivative_of_sin_of_pi(self):
        f = Derivative(sin)
        x = pi
        y = f(x)
        self.assertEqual(y, -1)

    def test_derivative_of_cos_of_pi(self):
        f = Derivative(cos)
        x = pi
        y = f(x)
        # self.assertEqual(round(y, 4), 0)
        self.assertEqual(y, 0)

    def test_second_derivative(self):
        f_1 = Derivative(sin)
        f_2 = Derivative(f_1)
        x = pi
        y = f_2(x)
        self.assertEqual(y, 0)

    def test_linear_fnx(self):
        def f(x):
            return m*x + b
        m = 3.5; b = 8
        f_1 = Derivative(f, h=0.5)
        self.assertEqual(f_1(4), m)
        self.assertEqual(f_1(1), m)
        self.assertEqual(f_1(0), m)
        f_2 = Derivative(f_1, h=0.5)
        self.assertEqual(f_2(4), 0)
        self.assertEqual(f_2(1), 0)
        self.assertEqual(f_2(0), 0)


    def test_symbolic_derivative(self):
        def g(x):
            return x**3
        def g_prime(x):
            return 3*x**2
        f = SymbolicDerivative(g)
        x = 2
        dydx = f(x)
        actual = g_prime(x)  # = 12
        self.assertEqual(dydx, actual)


if __name__ == '__main__':
    unittest.main()
