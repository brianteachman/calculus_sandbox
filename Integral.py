# https://en.wikipedia.org/wiki/Numerical_integration
# https://en.wikipedia.org/wiki/Newton%E2%80%93Cotes_formulas

def trapezoidal_rule(f, a, x, n):
    """
    Approximate the definite integral using The Trapezoidal Rule

    https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.trapz.html#scipy.integrate.trapz
    https://en.wikipedia.org/wiki/Trapezoid_rule
    """
    h = (x - a) / float(n)
    F = 0.5 * f(a)
    for i in range(1, n):
        F += f(a + i*h)
    F += 0.5 * f(x)
    F *= h
    return F


def simpsons_rule(f, a, b, n):
    """
    Approximate the definite integral of f over a to b by using the composite
    Simpson's rule, using n subintervals (with n even)

    https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.simps.html
    # https://en.wikipedia.org/wiki/Simpson%27s_rule
    """
    if n % 2:
        raise ValueError("n must be even (n=%d received)" % n)

    h = (b - a) / n
    s = f(a) + f(b)

    for i in range(1, n, 2):
        s += 4 * f(a + i * h)
    for i in range(2, n-1, 2):
        s += 2 * f(a + i * h)
    return s * h / 3


class Integral(object):
    """ To help ease working with numerical and symbolic calculus """

    def __init__(self, f, a, n=100):
        self.f, self.a, self.n = f, a, n

    def __call__(self, x):
        return trapezoidal_rule(self.f, self.a, x, self.n)
