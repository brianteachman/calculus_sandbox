"""

To help ease working with numerical and symbolic calculus

https://en.wikipedia.org/wiki/Numerical_differentiation
"""


def adf(f, x, h):
    """ Find derivative using The Approximate Differentiation Formula """
    return (f(x + h) - f(x)) / h


class Derivative(object):
    def __init__(self, f, h=1E-5):
        self.f = f
        self.h = float(h)

    def __call__(self, x):
        dydx = adf(self.f, x, self.h)
        return round(dydx, 4)  # exact to 7 decimal places


class SymbolicDerivative(object):
    def __init__(self, f):
        self.f = f

    def __call__(self, x):
        return self.symbolicdiff(x)

    def symbolicdiff(self, x):
        from sympy import Symbol, diff, lambdify
        sym_x = Symbol('x')
        sym_f = self.f(sym_x)
        df = diff(sym_f, sym_x)
        # convert symbolic function into numerical function
        f = lambdify([sym_x], df)
        return f(x)

