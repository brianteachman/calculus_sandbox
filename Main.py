import sympy as sym
from math import exp, pi, sin, cos, tan
from Integral import Integral


def f(x):
    return exp(-x**2) * sin(10*x)

a = 0
n = 200
F = Integral(f, a, n)
x = 2*pi
# print(F(x))

print("----------------------------------------------------")

F = Integral(sin, 0, 200)
x = 0
print(F(x))

print("----------------------------------------------------")

F = Integral(sin, 0, 200)
x = 1
print(F(x))

print("----------------------------------------------------")
"""  Using SymPy  """

# single variable calculus :)
x = sym.Symbol('x')

# define our f(x)
f_expr = sym.sin(x)
print(f_expr)

# integrate our f(x)
F_expr = sym.integrate(f_expr, x)
print(F_expr)

# convert the symbolic integral to a numerical function, F(x)
F = sym.lambdify([x], F_expr)

# examples using the integral F(x)
F_x = F(0);  print(F_x)
F_x = F(1);  print(F_x)

print("----------------------------------------------------")
""" Using SciPy

https://docs.scipy.org/doc/scipy/reference/tutorial/integrate.html
https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quad.html#scipy.integrate.quad
"""

print(dir())

print("----------------------------------------------------")
